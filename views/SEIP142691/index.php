<?php
include_once('../../vendor/autoload.php');

if(!isset($_SESSION) )session_start();
use App\Message\Message;


?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../resource/assets/js/bootstrap.min.js">
    <link rel="stylesheet" href="../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="ism/css/my-slider.css"/>
    <script src="ism/js/ism-2.2.min.js"></script>
    <link rel="stylesheet" href="../../resource/assets/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <div class="social-bookmark">
            <a href="https://www.facebook.com/istiyakaminsanto" target="_blank"><img src="../../resource/img/social_bookmark/facebook.png" alt=""></a>
            <a href="https://twitter.com/istiyakamin" target="_blank"><img src="../../resource/img/social_bookmark/twitter.png" alt=""></a>
            <a href="https://www.linkedin.com/in/istiyak-amin-s-06b0aa101" target="_blank"><img src="../../resource/img/social_bookmark/in.png" alt=""></a>
            <a href="https://www.instagram.com/istiyka_amin" target="_blank"><img src="../../resource/img/social_bookmark/instra.png" alt=""></a>
            <a href="https://plus.google.com/u/1/104189513778110467182" target="_blank"><img src="../../resource/img/social_bookmark/google.png" alt=""></a>
        </div>
        
        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li class="active"><a href="index.php">HOME</a></li>
                    <li><a href="login.php">LOG IN</a></li>
                    <li><a href="register.php">REGISTER</a></li>
                    <li><a href="#">ABOUT</a></li>
                    <li><a href="#">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <div class="ism-slider" data-transition_type="zoom" data-image_fx="zoompan" data-radio_type="thumbnail" id="my-slider">
            <ol>
                <li>
                    <img src="ism/image/slides/_u/1481286225790_668114.jpg">
                    <div class="ism-caption ism-caption-0">Atomic Project</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226577_96712.jpg">
                    <div class="ism-caption ism-caption-0">Book Title</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226369_604999.jpg">
                    <div class="ism-caption ism-caption-0">Birthday</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226032_873784.jpg">
                    <div class="ism-caption ism-caption-0">Gender</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286225301_740007.jpg">
                    <div class="ism-caption ism-caption-0">Email</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226543_137768.jpg">
                    <div class="ism-caption ism-caption-0">Hobbies</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226240_545874.jpg">
                    <div class="ism-caption ism-caption-0">City</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226770_47129.jpg">
                    <div class="ism-caption ism-caption-0">Profile Picture</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286227031_996989.jpg">
                    <div class="ism-caption ism-caption-0">Summary of Organization</div>
                </li>
            </ol>
        </div>



    </div>

    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
        echo "&nbsp;".Message::message();
    }
    Message::message(NULL);
    ?>




    <div>
        <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
    </div>
</div>
</body>
</html>
<!--<!DOCTYPE html>-->
<!--<html lang="en" xmlns="http://www.w3.org/1999/html">-->
<!--<head>-->
    <!--<meta charset="UTF-8">-->
    <!--<title>login form</title>-->
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0" />-->
    <!--<link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">-->
    <!--<link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">-->
    <!--<link rel="stylesheet" href="../../resource/assets/css/bootstrap.css">-->
    <!--<link rel="stylesheet" href="../../resource/assets/js/bootstrap.min.js">-->
    <!--<link rel="stylesheet" href="../../resource/assets/css/bootstrap-responsive.css">-->
    <!--<link rel="stylesheet" href="../../resource/assets/css/style.css">-->

<!--</head>-->
<!--<body>-->
<!--<div class="container">-->
    <!--<div class="container-fluid">-->

        <!--<header>-->
            <!--<div class="logo_area">-->
                <!--<p class="logo">Atomic Project</p>-->
                <!--<h3 class="subLogo">SEIP-142691 Batch-35</h3>-->
            <!--</div>-->
            <!--<nav class="navbar navbar-inner">-->
                <!--<ul class="nav">-->
                    <!--<li><a href="Birthday/index.php">BOOKTITLE</a></li>-->
                    <!--<li><a href="Birthday/index.php">BIRTHDAY</a></li>-->
                    <!--<li><a href="Gender/index.php">GENDER</a></li>-->
                    <!--<li><a href="Email/index.php">EMAIL</a></li>-->
                    <!--<li><a href="Hobbies/index.php">HOBBIES</a></li>-->
                    <!--<li><a href="City/index.php">CITY</a></li>-->
                    <!--<li><a href="ProfilePicture/index.php">PROFILE PICTURE</a></li>-->
                    <!--<li><a href="SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>-->
                <!--</ul>-->

            <!--</nav>-->
        <!--</header>-->
        <!--<div><img src="../../resource/img/Atomic-Industries-Slider-3.jpg" alt=""></div>-->


        <!--</div>-->









    <!--<div>-->
            <!--<footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>-->
        <!--</div>-->
    <!--</div>-->
<!--</body>-->
<!--</html>-->
