

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../../../resource/assets/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <div class="social-bookmark">
            <a href="https://www.facebook.com/istiyakaminsanto" target="_blank"><img src="../../../resource/img/social_bookmark/facebook.png" alt=""></a>
            <a href="https://twitter.com/istiyakamin" target="_blank"><img src="../../../resource/img/social_bookmark/twitter.png" alt=""></a>
            <a href="https://www.linkedin.com/in/istiyak-amin-s-06b0aa101" target="_blank"><img src="../../../resource/img/social_bookmark/in.png" alt=""></a>
            <a href="https://www.instagram.com/istiyka_amin" target="_blank"><img src="../../../resource/img/social_bookmark/instra.png" alt=""></a>
            <a href="https://plus.google.com/u/1/104189513778110467182" target="_blank"><img src="../../../resource/img/social_bookmark/google.png" alt=""></a>
        </div>
        
        

        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                    <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li class="active"><a href="../Hobbies/index.php">HOBBIES</a></li>
                    <li><a href="../City/index.php">CITY</a></li>
                    <li><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../resource/img/hobbies.jpg" alt=""></div>
        <p class="title">Hobbies</p>

        <?php
        require_once("../../../vendor/autoload.php");
        use App\Message\Message;

        if(!isset( $_SESSION)) session_start();
        echo Message::message();

        ?>

        <a href="../Hobbies/index.php">
            <button class="btn btn-primary">
                <i class="fa fa-list" aria-hidden="true"></i> All Hobbies list
            </button>
        </a>


        <fieldset class="control-group ">
            <legend>Add your Hobbies</legend>
            <form action="store.php" method="post" class="form-inline">
                <div class="input_form">
                    <label for="hobbies"> &nbsp;  &nbsp; Hobbies &nbsp; </label>
                    <label class="checkbox">
                        <input type="checkbox" name="hobbies[]" id="Checkbox1" value="programming"> Programming
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="hobbies[]" id="inlineCheckbox2" value="fishing"> Fishing
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="hobbies[]" id="inlineCheckbox3" value="hacking"> Hacking
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="hobbies[]" id="inlineCheckbox1" value="gardening"> Gardening
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="hobbies[]" id="inlineCheckbox2" value="racing"> Racing
                    </label>
                    <label class="checkbox">
                        <input type="checkbox" name="hobbies[]" id="inlineCheckbox3" value="playing"> Playing
                    </label>
                </div>
                <div class="input_form">
                    <label for="Author">User Name</label>
                    <input type="text" id="Author" class="input-xxlarge" name="author_name">
                </div>
                <div>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    <input type="submit" class="btn btn-success" value="submit">
                </div>
            </form>
        </fieldset>


    </div>
    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
</div>
</body>
</html>