<?php
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;
$auth= new Auth();
$status= $auth->prepare($_POST)->is_exist();
if($status){
    Message::setMessage("<div class='alert alert-danger'>
    <strong>Taken!</strong> Email has already been taken. </div>");
    return Utility::redirect('../register.php');
}else{
    $_POST['email_token'] = md5(uniqid(rand()));
    $obj= new User();
    $obj->prepare($_POST)->store();
    require '../../../vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 0;
    $mail->SMTPAuth   = true;
    $mail->SMTPSecure = "ssl";
    $mail->Host       = "smtp.gmail.com";
    $mail->Port       = 465;
    $mail->AddAddress($_POST['email']);
    $mail->Username="atomicprojectb35@gmail.com";               //       your gmail address
    $mail->Password="35projectatomic";                        //       your gmail password
    $mail->SetFrom('bitm.php22@gmail.com','User Management');
    $mail->AddReplyTo("bitm.php22@gmail.com","User Management");
    $mail->Subject    = "Your Account Activation Link";
    $message =  "Please click this link to verify your account: 
       http://localhost/BITM/Atomic_project_IstiyakAmin_142691_B35/views/SEIP142691/Controller/emailverification.php?email=".$_POST['email']."&email_token=".$_POST['email_token'];
    $mail->MsgHTML($message);
    $mail->Send();
}
