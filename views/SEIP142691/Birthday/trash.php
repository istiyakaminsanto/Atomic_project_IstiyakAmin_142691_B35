<?php
require_once("../../../vendor/autoload.php");

use App\Birthday\Birthday;
use App\Message\Message;
use App\Utility\Utility;



$objBirthday = new Birthday();


$allData = $objBirthday->trashed("obj");

$serial = 1;




######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBirthday->trashedPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../../../resource/assets/style.css">

    <script type="text/javascript">
        function confirmation() {
            var answer = confirm("Are you sure want to delete")
            if (answer){
                alert("You successfully deleted")
            }
            else{
                alert("You don't delete")
            }
        }

        function recover() {
            alert("You successfully recover from the trash list.if you want to see this  go to main list")
        }


    </script>

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <div class="social-bookmark">
            <a href="https://www.facebook.com/istiyakaminsanto" target="_blank"><img src="../../../resource/img/social_bookmark/facebook.png" alt=""></a>
            <a href="https://twitter.com/istiyakamin" target="_blank"><img src="../../../resource/img/social_bookmark/twitter.png" alt=""></a>
            <a href="https://www.linkedin.com/in/istiyak-amin-s-06b0aa101" target="_blank"><img src="../../../resource/img/social_bookmark/in.png" alt=""></a>
            <a href="https://www.instagram.com/istiyka_amin" target="_blank"><img src="../../../resource/img/social_bookmark/instra.png" alt=""></a>
            <a href="https://plus.google.com/u/1/104189513778110467182" target="_blank"><img src="../../../resource/img/social_bookmark/google.png" alt=""></a>
        </div>

        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                    <li class="active"><a href="../Birthday/index.php">BIRTHDAY</a></li>
                    <li><a href="../Gender/index.php">GENDER</a></li>
                    <li><a href="../Email/index.php">EMAIL</a></li>
                    <li><a href="../Hobbies/index.php">HOBBIES</a></li>
                    <li><a href="../City/index.php">CITY</a></li>
                    <li><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                    <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                </ul>

            </nav>
        </header>
        <div><img src="../../../resource/img/birthday.jpg" alt=""></div>
        <p class="title">Birthday</p>

        <a href="../Birthday/index.php">
            <button class='btn btn-primary'>
                <i class="fa fa-list" aria-hidden="true"></i> All Date Of Birth
            </button>
        </a>

        <div class="input-group col-md-12" style="float: right">
            <input type="text" class="  search-query form-control" placeholder="Search" />
                    <span class="input-group-btn">
                        <button class="btn btn-danger" type="button">
                            <span class="fa fa-search"></span>
                        </button>
                    </span>
        </div>



        <div class="container-fluid>
        <div class="table-responsive>
            <table border=2 class="table table-bordered">
                <th> Serial </th><th> ID </th><th> Date of Birth </th><th> Name </th><th> Action </th>
                <?php foreach($someData as $oneData){

                    echo "<tr>";
                    echo "<td> $serial </td>";
                    echo "<td> $oneData->id </td>";
                    echo "<td> $oneData->birthday</td>";
                    echo "<td> $oneData->author_name </td>";

                    echo "
                        <td>
                            <a href='Views.php?id=$oneData->id'>
                                <button class='btn btn-info'>
                                   <i class=\"fa fa-search-plus\" aria-hidden=\"true\"></i> View
                                </button>
                            </a>
                            <a href='edit.php?id=$oneData->id'>
                                <button class='btn btn-primary'>
                                   <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> Edit
                                </button>
                            </a>
                            <a href='recover.php?id=$oneData->id'>
                                <button class='btn btn-warning' onclick='recover()'>
                                   <i class=\"fa fa-refresh\" aria-hidden=\"true\"></i> Recover
                                </button>
                            </a>
                            <a href='delete.php?id=$oneData->id'>
                                <button class='btn btn-danger' onclick='confirmation()'>
                                    <i class='fa fa-trash' aria-hidden='true'></i> Delete
                                </button>
                            </a>
            
                        </td>
    
                ";


                    echo "</tr>";
                    $serial++;
                }?>

            </table>

        </div>
    </div>

    <!--  ######################## pagination code block#2 of 2 start ###################################### -->

    <div class="pagination-centered">
        <nav class=" pagination">
            <ul class="nav">

                <?php

                $pageMinusOne  = $page-1;
                $pagePlusOne  = $page+1;
                if($page>$pages) Utility::redirect("index.php?Page=$pages");

                if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
                for($i=1;$i<=$pages;$i++)
                {
                    if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                    else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                }
                if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                ?>
            </ul>
        </nav>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;">
            <?php
            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>

    </div>
    <!--  ######################## pagination code block#2 of 2 end ###################################### -->


    <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>

</div>
</body>
</html>
