<?php
require_once("../../../vendor/autoload.php");

use App\ProfilePicture\ProfilePicture;
use App\Message\Message;
use App\Utility\Utility;


if(!isset($_SESSION) )session_start();
use App\User\User;
use App\User\Auth;

$obj= new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->prepare($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('../login.php');
    return;
}


$objProfilePicture = new ProfilePicture();
$allData = $objProfilePicture->index("obj");
$serial=0;
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/js/bootstrap.min.js">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../ism/css/my-slider.css"/>
    <script src="../ism/js/ism-2.2.min.js"></script>
    <link rel="stylesheet" href="../../../resource/assets/style.css">

    <style>
        <?php foreach ($allData as $oneData) {?>
        #my-slider .ism-radios li.ism-radio-<?php echo $serial++; ?> label {
            background-image: url("img/<?php echo $oneData->profile_picture ;?>");
        }
        <?php }?>
    </style>

</head>
<body>
    <div class="container">
        <div class="container-fluid">

            <div class="social-bookmark">
                <a href="https://www.facebook.com/istiyakaminsanto" target="_blank"><img src="../../../resource/img/social_bookmark/facebook.png" alt=""></a>
                <a href="https://twitter.com/istiyakamin" target="_blank"><img src="../../../resource/img/social_bookmark/twitter.png" alt=""></a>
                <a href="https://www.linkedin.com/in/istiyak-amin-s-06b0aa101" target="_blank"><img src="../../../resource/img/social_bookmark/in.png" alt=""></a>
                <a href="https://www.instagram.com/istiyka_amin" target="_blank"><img src="../../../resource/img/social_bookmark/instra.png" alt=""></a>
                <a href="https://plus.google.com/u/1/104189513778110467182" target="_blank"><img src="../../../resource/img/social_bookmark/google.png" alt=""></a>
            </div>

            <header>
                <div class="logo_area">
                    <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                    <h3 class="subLogo cssanimation fadeInRight">SEIP-142691 Batch-35</h3>
                </div>
                <nav class="navbar navbar-inner">
                    <ul class="nav">
                        <li><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                        <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                        <li><a href="../Gender/index.php">GENDER</a></li>
                        <li><a href="../Email/index.php">EMAIL</a></li>
                        <li><a href="../Hobbies/index.php">HOBBIES</a></li>
                        <li><a href="../City/index.php">CITY</a></li>
                        <li class="active"><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                        <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                    </ul>
                </nav>
            </header>

            <p class="title">Dynamic Carosol</p>

            <div class="ism-slider" data-transition_type="zoom" data-image_fx="zoompan" data-radio_type="thumbnail" id="my-slider">
                <ol>
                <?php foreach ($allData as $oneData) {?>
                    <li>
                        <img src="img/<?php echo $oneData->profile_picture ?>" >
                        <div class="ism-caption ism-caption-0">User name =<?php echo $oneData->author_name; ?></div>
                    </li>
                 <?php }?>
                </ol>
            </div>
        </div>
            <div>
                <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
            </div>
        </div>

</body>
</html>