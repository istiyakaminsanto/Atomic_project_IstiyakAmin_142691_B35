<?php
include_once('../../vendor/autoload.php');

if(!isset($_SESSION) )session_start();
use App\Message\Message;


?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>login form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../resource/assets/js/bootstrap.min.js">
    <link rel="stylesheet" href="../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="ism/css/my-slider.css"/>
    <script src="ism/js/ism-2.2.min.js"></script>
    <link rel="stylesheet" href="../../resource/assets/style.css">

</head>
<body>
<div class="container">
    <div class="container-fluid">

        <div class="social-bookmark">
            <a href="https://www.facebook.com/istiyakaminsanto" target="_blank"><img src="../../resource/img/social_bookmark/facebook.png" alt=""></a>
            <a href="https://twitter.com/istiyakamin" target="_blank"><img src="../../resource/img/social_bookmark/twitter.png" alt=""></a>
            <a href="https://www.linkedin.com/in/istiyak-amin-s-06b0aa101" target="_blank"><img src="../../resource/img/social_bookmark/in.png" alt=""></a>
            <a href="https://www.instagram.com/istiyka_amin" target="_blank"><img src="../../resource/img/social_bookmark/instra.png" alt=""></a>
            <a href="https://plus.google.com/u/1/104189513778110467182" target="_blank"><img src="../../resource/img/social_bookmark/google.png" alt=""></a>
        </div>

        <header>
            <div class="logo_area">
                <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                <h3 class="subLogo cssanimation fadeInRight">SEIP-142691 Batch-35</h3>
            </div>
            <nav class="navbar navbar-inner">
                <ul class="nav">
                    <li><a href="index.php">HOME</a></li>
                    <li><a href="login.php">LOG IN</a></li>
                    <li class="active"><a href="register.php">REGISTER</a></li>
                    <li><a href="#">ABOUT</a></li>
                    <li><a href="#">CONTACT</a></li>
                </ul>
            </nav>
        </header>
        <div class="ism-slider" data-transition_type="zoom" data-image_fx="zoompan" data-radio_type="thumbnail" id="my-slider">
            <ol>
                <li>
                    <img src="ism/image/slides/_u/1481286225790_668114.jpg">
                    <div class="ism-caption ism-caption-0">Atomic Project</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226577_96712.jpg">
                    <div class="ism-caption ism-caption-0">Book Title</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226369_604999.jpg">
                    <div class="ism-caption ism-caption-0">Birthday</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226032_873784.jpg">
                    <div class="ism-caption ism-caption-0">Gender</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286225301_740007.jpg">
                    <div class="ism-caption ism-caption-0">Email</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226543_137768.jpg">
                    <div class="ism-caption ism-caption-0">Hobbies</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226240_545874.jpg">
                    <div class="ism-caption ism-caption-0">City</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286226770_47129.jpg">
                    <div class="ism-caption ism-caption-0">Profile Picture</div>
                </li>
                <li>
                    <img src="ism/image/slides/_u/1481286227031_996989.jpg">
                    <div class="ism-caption ism-caption-0">Summary of Organization</div>
                </li>
            </ol>
        </div>
    </div>

    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
        echo "&nbsp;".Message::message();
    }
    Message::message(NULL);
    ?>


    <div class="container-fluid">
    <div class="col-sm-1 middle-border"></div>
    <div class="col-sm-1"></div>

    <div class="col-sm-5">

        <div class="form-box" style="margin-top: 0%">
            <div class="form-top">
                <div class="form-top-left">
                    <h3 class="title">Sign up now</h3>
                    <p>Fill in the form below to get instant access:</p>
                </div>
                <div class="form-top-right">
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="Controller/registration.php" method="post" class="registration-form">
                    <div class="form-group">
                        <label class="sr-only" for="form-first_name">First name</label>
                        <input type="text" name="first_name" placeholder="First name..." class="input-xxlarge form-first-name form-control" id="form-first-name" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-last-name">Last name</label>
                        <input type="text" name="last_name" placeholder="Last name..." class="input-xxlarge form-last-name form-control" id="form-last-name" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-email">Email</label>
                        <input type="text" name="email" placeholder="Email..." class="input-xxlarge form-email form-control" id="form-email" required>
                    </div>

                    <div class="form-group">
                        <label class="sr-only" for="form-password">Password</label>
                        <input type="password" name="password" placeholder="Password..." class="input-xxlarge form-password form-control" id="form-password" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-email">Phone</label>
                        <input type="text" name="phone" placeholder="Phone..." class="input-xxlarge form-phone form-control" id="form-phone" required>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="address">Address</label>
                        <input type="text" name="address" placeholder="Address..." class="input-xxlarge form-address form-control" id="form-address" required>
                    </div>
                    <button type="submit" class="btn btn-success">Sign me up!</button>
                </form>
            </div>
        </div>
    </div>

    </div>

    <div>
        <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
    </div>
</div>
</body>
</html>