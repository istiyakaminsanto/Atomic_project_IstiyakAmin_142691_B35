
<?php
require_once("../../../vendor/autoload.php");
use App\Utility\Utility;

use App\BookTitle\BookTitle;
use App\Message\Message;

if(!isset($_SESSION) )session_start();
use App\User\User;
use App\User\Auth;

$obj= new User();
$obj->prepare($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->prepare($_SESSION)->logged_in();

if(!$status) {
    Utility::redirect('../login.php');
    return;
}



$objBookTitle = new BookTitle();


$allData = $objBookTitle->index("obj");

$serial = 1;




################## search  block 1 of 5 start ##################

if(isset($_REQUEST['search']) )$allData =  $objBookTitle->search($_REQUEST);
$availableKeywords=$objBookTitle->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';

################## search  block 1 of 5 end ##################



######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 5;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################



################## search  block 2 of 5 start ##################

if(isset($_REQUEST['search']) ) {
    $someData = $objBookTitle->search($_REQUEST);
    $serial = 1;
}
################## search  block 2 of 5 end ##################


?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Book Title</title>
    <link rel="icon"
          type="image/png"
          href="../../../resource/assets/icon/book/book-24-179933.png">
    <link rel="stylesheet" href="../../../font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Yellowtail" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/assets/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="../../../resource/assets/css/cssanimation.css">
    <link rel="stylesheet" href="../../../resource/assets/style.css">
    <style>
       
    </style>


    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resource/assets/css/jquery-ui.css">
    <script src="../../../resource/assets/js/jquery-1.12.4.js"></script>
    <script src="../../../resource/assets/js/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->

    <script type="text/javascript">
        function confirmation() {
            var answer = confirm("Are you sure want to delete")
            if (answer){
                alert("You successfully deleted")
            }
            else{
                alert("You don't delete")
            }
        }

        function softDelete() {
            alert("You successfully deleted from the list.if you want to recover it go to trash list")
        }

    </script>


</head>
<body>

    <div class="container">
        <div class="container-fluid">
            <div class="social-bookmark">
                <a href="https://www.facebook.com/istiyakaminsanto" target="_blank"><img src="../../../resource/img/social_bookmark/facebook.png" alt=""></a>
                <a href="https://twitter.com/istiyakamin" target="_blank"><img src="../../../resource/img/social_bookmark/twitter.png" alt=""></a>
                <a href="https://www.linkedin.com/in/istiyak-amin-s-06b0aa101" target="_blank"><img src="../../../resource/img/social_bookmark/in.png" alt=""></a>
                <a href="https://www.instagram.com/istiyka_amin" target="_blank"><img src="../../../resource/img/social_bookmark/instra.png" alt=""></a>
                <a href="https://plus.google.com/u/1/104189513778110467182" target="_blank"><img src="../../../resource/img/social_bookmark/google.png" alt=""></a>
            </div>

            <header>
                <div class="logo_area">
                    <p class="logo cssanimation fadeInLeft">Atomic Project</p>
                    <h3 class="subLogo cssanimation fadeInRight">SEIP-142691 Batch-35</h3>
                </div>
                <nav class="navbar navbar-inner">
                    <ul class="nav">
                        <li class="active"><a href="../BookTitle/index.php">BOOKTITLE</a></li>
                        <li><a href="../Birthday/index.php">BIRTHDAY</a></li>
                        <li><a href="../Gender/index.php">GENDER</a></li>
                        <li><a href="../Email/index.php">EMAIL</a></li>
                        <li><a href="../Hobbies/index.php">HOBBIES</a></li>
                        <li><a href="../City/index.php">CITY</a></li>
                        <li><a href="../ProfilePicture/index.php">PROFILE PICTURE</a></li>
                        <li><a href="../SummaryOfOrganization/index.php">SUMMARY OF ORGANIZATION</a></li>
                    </ul>

                </nav>
            </header>
                    <!--          login messege start     -->
            <div><img src="../../../resource/img/booktitle.jpg" alt=""></div>
                <p class="title">Booktitle</p>

            <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                echo "&nbsp;".Message::message();
            }
            Message::message(NULL);

            ?>
                    <!--          login messege end      -->

            <div class="container-fluid">
                <a href="../BookTitle/create.php">
                    <button class='btn btn-primary'>
                        <i class="fa fa-plus-square" aria-hidden="true"></i> Add New
                    </button>
                </a>
                <a href="trash.php">
                    <button class='btn btn-primary'>
                        <i class="fa fa-list" aria-hidden="true"></i> Trash List
                    </button>
                </a>
                <a href="pdf.php">
                    <button class='btn btn-primary'>
                        <i class="fa fa-book" aria-hidden="true"></i> Download as PDF
                    </button>
                </a>
                <a href="xl.php">
                    <button class='btn btn-primary'>
                        <i class="fa fa-file-excel-o" aria-hidden="true"></i> Download as XL
                    </button>
                </a>
                <a href="email.php?list=1">
                    <button class='btn btn-primary'>
                        <i class="fa fa-send" aria-hidden="true"></i> Email to friend
                    </button>
                </a>
                


                <!-- required for search, block 4 of 5 start -->

                <div class="input-group col-md-12" style="float: right">

                    <form id="searchForm" action="index.php"  method="get">
                        <input type="text" value="" id="searchID" class="search-query " name="search" placeholder="Search" >
                        <span class="input-group-btn">
                            <button class="btn btn-danger" type="button" value="search">
                                <span class="fa fa-search"></span>
                            </button>
                        </span><br>
                        <input type="checkbox"  name="byTitle"   checked  >By Title
                        <input type="checkbox"  name="byAuthor"  checked >By Author
                    </form>

                </div>
                <!-- required for search, block 4 of 5 end -->

            </div>
                
            <div class="container-fluid">
                <div class="table-responsive">
                    <table border=2 class="table table-bordered">
                        <th> Serial </th>
                        <th> ID </th>
                        <th> Book Title </th>
                        <th> Author Name </th>
                        <th> Action </th>
                            <?php foreach($someData as $oneData){

                                echo "<tr>";
                                    echo "<td> $serial </td>";
                                    echo "<td> $oneData->id </td>";
                                    echo "<td> $oneData->book_title </td>";
                                    echo "<td> $oneData->author_name </td>";

                                    echo "
                                       <td>
                                             <a href='Views.php?id=$oneData->id'>
                                                <button class='btn btn-info'>
                                                    <i class=\"fa fa-search-plus\" aria-hidden=\"true\"></i> View
                                                </button>
                                             </a>
                                             <a href='edit.php?id=$oneData->id'>
                                                <button class='btn btn-primary'>
                                                    <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i> Edit
                                                </button>
                                             </a>
                                             <a href='softDelete.php?id=$oneData->id'>
                                                <button class='btn btn-warning' onclick='softDelete()' >
                                                    <i class=\"fa fa-minus-square\" aria-hidden=\"true\"></i> Soft Delete
                                                </button>
                                             </a>
                                             <a href='delete.php?id=$oneData->id'>
                                                <button class='btn btn-danger' onclick='confirmation()'>
                                                    <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> Delete
                                                </button>
                                             </a>
                                        </td>
            
                                     ";


                                 echo "</tr>";
                                $serial++;
                                } ?>
                    </table>

                </div>
            </div>
        </div>



        <!--  ######################## pagination code block#2 of 2 start ###################################### -->

        <div class="pagination-centered">
            <nav class=" pagination">
                <ul class="nav">

                    <?php

                    $pageMinusOne  = $page-1;
                    $pagePlusOne  = $page+1;
                    if($page>$pages) Utility::redirect("index.php?Page=$pages");

                    if($page>1)  echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";
                    for($i=1;$i<=$pages;$i++)
                    {
                        if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
                        else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

                    }
                    if($page<$pages) echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

                    ?>
                </ul>
            </nav>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;">
                <?php
                if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

                if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
                else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

                if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
                else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
                ?>
            </select>
            
            <div style="float: right" class="container-fluid"><a href="../logout.php"><button class="btn btn-inverse">logout</button></a></div>

        </div>
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->


            <div>
                <footer class="modal-footer">@copyright IstiyakAmin All data reserved </footer>
            </div>
        </div>

</body>
</html>




<!-- required for search, block 5 of 5 start -->
<script>

    $(function() {
        var availableTags = [

            <?php
            echo $comma_separated_keywords;
            ?>
        ];
        // Filter function to search only from the beginning of the string
        $( "#searchID" ).autocomplete({
            source: function(request, response) {

                var results = $.ui.autocomplete.filter(availableTags, request.term);

                results = $.map(availableTags, function (tag) {
                    if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
                        return tag;
                    }
                });

                response(results.slice(0, 15));

            }
        });


        $( "#searchID" ).autocomplete({
            select: function(event, ui) {
                $("#searchID").val(ui.item.label);
                $("#searchForm").submit();
            }
        });


    });

</script>
<!-- required for search, block5 of 5 end -->

