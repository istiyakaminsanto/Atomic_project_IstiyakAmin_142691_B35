-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 04, 2016 at 07:58 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b35_ia`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `birthday`, `author_name`, `is_deleted`) VALUES
(11, '0000-00-00', '222', '2016-11-24 16:06:18'),
(14, '1999-12-03', 'Istiyak Amin', '2016-11-24 16:06:25'),
(16, '2016-11-13', 'Masum Howlader', 'No'),
(17, '0000-00-00', 'Prantom', '2016-11-24 16:06:30'),
(18, '2016-11-15', 'pi', '2016-11-24 16:06:33'),
(19, '2016-11-22', 'Roksana Begum2', '2016-11-24 16:06:35'),
(20, '2016-11-21', 'Istiyak Amin2', '2016-11-24 16:06:38'),
(21, '2016-11-23', 'pi', 'No'),
(22, '2016-11-11', 'Rabeya Akter', 'No'),
(23, '2016-09-12', 'Joya Akter', 'No'),
(24, '2016-11-30', 'Kiran Sharma', 'No'),
(25, '2016-11-14', 'asdfghjkl', 'No'),
(26, '2016-11-09', 'istiyak', 'No'),
(27, '0000-00-00', '', 'No'),
(28, '0000-00-00', '', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(7, 'ICT', 'Kajol Rani', 'No'),
(10, 'web desing2', 'md rasel2', '2016-11-24 16:18:37'),
(11, 'Sesh prohor', 'Shukanto vottacharjo', '2016-11-24 16:18:39'),
(19, 'Mathmatics-2', 'Sikander Ali', '2016-11-24 16:18:45'),
(20, 'Sonar Tori', 'Ravi Thagore', '2016-11-24 16:18:47'),
(25, 'Electronics', 'Samin Ahmed', 'No'),
(27, 'Bangla 1st paper', 'Istiyak Amin', 'No'),
(30, 'Bangla 2nd paper', 'pi', '2016-11-22 19:47:07'),
(31, 'lol and company', 'Istiyak Amin', 'No'),
(32, 'gitanjali', 'Roksana Begum2', 'No'),
(33, 'Electronics', 'pi', '2016-12-04 01:33:43'),
(34, 'Sahitto', 'Ali akbar', '2016-11-29 18:27:30'),
(35, 'Hajar bochor dhore', 'Munir chowdhuri', 'No'),
(37, 'Physics', 'Jani na', 'No'),
(38, 'chemistry', 'Rosaiyon', 'No'),
(39, 'Business Math', 'Abuler ma', 'No'),
(40, 'Kaktarua', 'Selina Rahman', 'No'),
(41, 'kakatua', 'fardoys Alom', 'No'),
(42, 'Rosh Alo', 'simu nacher', 'No'),
(44, 'Boyal khali', 'sokina begum', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `author_name`, `is_deleted`) VALUES
(1, 'Chittagong', 'Istiyak Amin', '2016-11-24 16:45:59'),
(4, 'Khulna', 'Hridoy', '2016-11-19 00:28:18'),
(6, 'Rangpur', 'Riders', '2016-11-19 00:22:31'),
(7, 'Barisal', 'pi', '2016-11-24 16:34:03'),
(8, 'Khulna', 'Roksana Begum2', '2016-11-24 16:34:09'),
(9, 'Dhaka', 'Istiyak Amin', 'No'),
(10, 'Rajshahi', 'Mithun', 'No'),
(11, 'Barisal', 'Sakima', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `author_name`, `is_deleted`) VALUES
(2, 'ctgnazrulislam@gmail.com', 'Md.Nazrul Islam', '2016-11-24 20:25:37'),
(5, 'rabeya@gmail.com', 'Rabeya Akter', 'No'),
(7, 'istiyakaminsanto@gmail.com', 'Istiyak Amin', 'No'),
(8, 'istiyakaminsanto22@gmail.com', 'pi', 'No'),
(9, 'istiyakaminsanto2@gmail.com', 'Roksana Begum2', 'No'),
(10, 'istiyakaminsanto2hj@gmail.com', 'ravindranath thagore', 'No'),
(11, 'istiyakaminsantokj2@gmail.com', 'pi', 'No'),
(12, 'istiyakaminsanto@gmail.com', 'Istiyak Amin', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender`, `author_name`, `is_deleted`) VALUES
(4, 'Male', 'Istiyak Amin', '2016-11-21 00:39:05'),
(7, 'Male', 'ravindranath thagore', 'yes'),
(8, 'Male', 'Arafatul islam', '2016-11-26 16:02:51'),
(10, 'Female', 'Naznin Akter', '2016-11-22 01:26:10'),
(11, 'Female', 'Tania Akter', '2016-11-19 00:35:38'),
(12, 'Male', 'Roksana Begum', 'No'),
(14, 'Female', 'Roksana Begum40', 'No'),
(16, 'Female', 'Istiyak Amin', 'No'),
(17, 'Male', 'Moslem Uddin', 'No'),
(18, 'Female', 'Roksana Begum2', 'No'),
(19, 'Female', 'Roksana Begum2', 'No'),
(20, 'Male', 'pi', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobbies`, `author_name`, `is_deleted`) VALUES
(1, 'programming,fishing,hacking', 'Istiyak Amin', 'No'),
(2, 'fishing,hacking,gardening,racing,playing', 'ravindranath thagore', '2016-11-24 17:51:02'),
(5, 'fishing,gardening', 'md rasel', '2016-11-24 17:50:10'),
(6, 'programming,fishing,hacking,gardening,racing,playing', 'Roksana Begum', '2016-11-24 17:50:01'),
(7, 'hacking,gardening,racing,playing', 'ravindranath thagore', '2016-11-24 17:49:58'),
(10, 'playing', 'Roksana Begum2', '2016-11-24 17:27:27'),
(11, 'programming', 'asdfghjk', 'No'),
(12, 'fishing,hacking', 'dfghjkl', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(200) NOT NULL,
  `profile_picture` varchar(230) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `profile_picture`, `author_name`, `is_deleted`) VALUES
(5, '1479754092Screenshot (40).png', 'Istiyak Amin', '2016-11-24 17:45:36'),
(7, '1479651799Screenshot (35).png', 'Istiyak Amin2', '2016-11-24 17:46:14'),
(8, '1479651807Screenshot (39).png', 'pi', '2016-11-24 17:46:18'),
(9, '1479651817Screenshot (33).png', 'Roksana Begum2', '2016-11-24 17:46:21'),
(10, '1479745971gender01.png', 'pi', '2016-11-24 17:46:24'),
(11, '1479754020Screenshot (33).png', 'ravindranath thagore', 'No'),
(12, '1479987967ans.PNG', 'Istiyak Amin', 'No'),
(13, '148006897514572202_731456503660275_5276188903422799702.jpg', 'Istiyak Amin', 'No'),
(14, '14800689959576_1730495683900725_3574721519909240297_n.jpg', 'Tuhin', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(200) NOT NULL,
  `summary_of_organization` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(200) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `summary_of_organization`, `author_name`, `is_deleted`) VALUES
(5, 'this site is very simple', 'Aminul Islam2', '2016-11-24 17:40:45'),
(6, 'this is very bad', 'Emon Ali', '2016-11-24 17:40:47'),
(8, 'it''s a summary of organization', 'Md.Nazrul Islam', '2016-11-24 16:08:42'),
(9, '01862864710', 'ravindranath thagore', '2016-11-24 16:08:45'),
(10, 'tyhtyhty', 'hyjtyuhtyh', '2016-11-24 16:08:49'),
(11, 'yjtyuhjj', 'jyuyjtj', 'No'),
(12, 'sdfghjk', 'asdfghjkl', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(17, 'Test', 'User', 'tushar.chowdhury@gmail.com', '202cb962ac59075b964b07152d234b70', '01711111111', 'Chittagong', 'Yes'),
(18, 'sdjf', 'lksdjf', 'tusharbd@gmail.com', 'b518d60581cfcd1c861145739d4666d6', '5235', 'dfgdg', 'Yes'),
(19, 'asfds', 'sdfgs', 'x@y.z', '202cb962ac59075b964b07152d234b70', '4545', 'sfsj', '4ae15d1c46f25be8db9d07061463c5f0'),
(24, 'Istiyak', 'Amin', 'istiyakaminsanto@yahoo.com', '4a7d1ed414474e4033ac29ccb8653d9b', '00', 'a', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
